<?php
	/*
	Plugin Name: myPlugin
	Description: Testing
	Version: 1.0
	Author: openKatta
	License: GPLv3
	*/

	register_activation_hook(__FILE__, 'createDatabase');

	function createDatabase(){
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			global $wpdb;
			$tableName = $wpdb->prefix."myPlugin";
			$charset_collate = $wpdb->get_charset_collate();

			$sql = "CREATE TABLE IF NOT EXISTS `$tableName`(
					ID INT NOT NULL AUTO_INCREMENT,
					userName VARCHAR(100) NOT NULL,
					userEmail VARCHAR(100) NOT NULL,
					PRIMARY KEY (`ID`))
					ENGINE = InnoDB
					$charset_collate;";
			dbDelta($sql);
	}

	add_action('admin_menu', 'myPage_Create');

	function myPage_Create(){
		add_menu_page('myPlugin', 'myPlugin', 'manage_options', __FILE__, 'myPluginPage');
		add_submenu_page(__FILE__, __("Home", "myPlugin"), __("Home", "myPlugin"), 'manage_options', __FILE__. '_Home', 'HomePage');
	}

	function myPluginPage(){
		_e("<h2>Welcome to my Plugin!</h2>", "myPlugin");
	}

	function HomePage(){
		?>
		<form action="#" method="post">
   			<br/><label>Name  	</label><input type="text" name="username"  class="dirty" ><span class="highlight"></span><span class="bar">	
    		<br/><br/><label>Email&nbsp;	</label><input type="email" name="emailid"  class="dirty" ><span class="highlight"></span><span class="bar">
  			<br/><br/><input type="button" class="button-primary" value="submit">
		</form>
		<?php
	}
?>